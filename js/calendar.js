 //Author: Akshay Jain
 //Purpose: Creating a calendar using ReactJs
 //Date: Aug 01, 2016

//Contains methods related to actions performed on calendar
 var Calendar = React.createClass({                           
        
        //Initial state of calendar
        getInitialState: function() {
          return {
            month: this.props.selected.clone(),
            year: this.props.selected.clone(),
            selected: this.props.selected.clone()
          };
        },

        //Clicking on previous button
        previous: function() {
          var month = this.state.month;
          month.add(-1, "M");
          this.setState({ month: month });
        },
        
        //Clicking on next button
        next: function() {
          var month = this.state.month;
          month.add(1, "M");
          this.setState({ month: month });
        },
        
        //Selecting Date
        select: function(day) {
          this.state.selected = day.date;
          this.forceUpdate();
        },
        
        //Changing month by dropdown
        changeMonth: function(e) {
          this.setState({ month: moment().month(e.target.value).year(this.state.month.year()) });
        },

        //Changing year by dropdown
        changeYear: function(e) {
          this.setState({ month: moment().month(this.state.month.month()).year(e.target.value) });
        },

        //Render Calendar UI
        render: function() {
          var next = ">>";
          var previous = "<<";
          return <div className="container">
            {this.renderSelectedDate()}
            {this.renderTop()}
            <div className="header">
              <i className="arrow arrow-left" onClick={this.previous}>{previous}</i>
              {this.renderMonthLabel()}
              <i className="arrow arrow-right" onClick={this.next}>{next}</i>
            </div>
            <DayNames />
            {this.renderWeeks()}
          </div>;
        },

        //Render top header of UI
        renderTop : function(){
          return <div className="topSelect">
            {this.renderMonths()}
            {this.renderYears()}
        </div>;
        },

        //Method to get selected date
        renderSelectedDate : function(){
          document.location.hash =  moment(this.state.selected.toString()).format('MM-DD-YYYY');
          return <div className="selected-date">
            {moment(this.state.selected.toString()).format('MMMM DD, YYYY')}
        </div>;
        },

        //Render Weeks
        renderWeeks: function() {
          var weeks = [],
            done = false,
            date = this.state.month.clone().startOf("month").add("w" -1).day("Sunday"),
            monthIndex = date.month(),
            count = 0;
            while (!done) {
              weeks.push(<Week key={date.toString()} date={date.clone()} month={this.state.month} select={this.select} selected={this.state.selected} />);
              date.add(1, "w");
              done = count++ > 2 && monthIndex !== date.month();
              monthIndex = date.month();
            }
          return weeks;
        },

        //Render Months for dropdown
        renderMonths: function() {
          var monthOptions = [],
            months = moment.months();
            for (var i = 0; i < 12; i++) {
              monthOptions[i] = <option key={i} value={months[i]}>{months[i]}</option>;
            }
          return <select ref="monthsSelect"  onChange={this.changeMonth} value={months[this.state.month.month()]} className="months">
                    {monthOptions}
                  </select>;
        },

        //Render Years for dropdown
        renderYears: function() {
          var yearOptions = [],
            months = moment.months();
            for (var i = 1970; i < 2100; i++) {
              yearOptions[i] = <option key={i} value={i}>{i}</option>;
            }
          return <select onChange={this.changeYear} value={this.state.month.year()} className="years">
                    {yearOptions}
                  </select>;
        },

        //Display selected month with year
        renderMonthLabel: function() {
          return <div className="render-month">{this.state.month.format("MMMM, YYYY")}</div>;
        },
      });
      
      //Class object for Days to be displayed on UI
      var DayNames = React.createClass({
        render: function() {
          return <div className="week names">
            <div className="day">Sun</div>
            <div className="day">Mon</div>
            <div className="day">Tue</div>
            <div className="day">Wed</div>
            <div className="day">Thu</div>
            <div className="day">Fri</div>
            <div className="day">Sat</div>
          </div>;
        }
      });
      
      //Display Dates
      var Week = React.createClass({
        render: function() {
          var days = [],
            date = this.props.date,
            month = this.props.month;
            for (var i = 0; i < 7; i++) {
              var day = {
                  name: date.format("dd").substring(0, 1),
                  number: date.date(),
                  isCurrentMonth: date.month() === month.month(),
                  isToday: date.isSame(new Date(), "day"),
                  date: date
              };
              days.push(<div key={day.date.toString()} className={"day" + (day.isToday ? " today" : "") + (day.isCurrentMonth ? "" : " different-month") + (day.date.isSame(this.props.selected) ? " selected" : "")} onClick={this.props.select.bind(null, day)}>{day.number}</div>);
              date = date.clone();
              date.add(1, "d");
            }
          return <div className="week" key={days[0].toString()}>
            {days}
          </div>;
        }
      });

      //Initail method to check the valid date in parameters
      var initailDate = function(){
          var URL = document.location.hash;
          if(URL != "") {
              URL = URL.replace("#","");
              var isValid = moment(URL, 'MM-DD-YYYY').isValid();
              if(isValid) {
                return  moment(URL);
              } else {
                window.location.hash = "";
                return moment().startOf("day");
              }
          } else {
              return moment().startOf("day");
          }
      }

      //Render the whole calendar with default date as today date
      ReactDOM.render(<Calendar selected={initailDate()} />, document.getElementById("calendar"));

/*
// Creation Date      : Aug 01, 0216
// Developer          : Akshay Jain
// File               : Calendar.Js
// Purpose            : Creating a responsive calendar using reactJs
// Input parameters   : Specific for each function
// Output parameters  : Specific for each function
// Last modified      : Aug 03, 2016
*/ 