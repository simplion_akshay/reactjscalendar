<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width" />
    <title>Calendar</title>
    <link rel="stylesheet" href="css/style.css" />
  </head>
  <body>
    <h1>Calendar</h1>
    <div id="calendar">
     
    </div>
    
    <script src="js/react.js"></script>
    <script src="js/react-dom.js"></script>
    <script src="js/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/babel-core/5.8.24/browser.min.js"></script>
    <script  type="text/babel" src="js/calendar.js"></script>
  </body>
</html>

<!--
// Creation Date      : Aug 01, 2016
// Developer          : Akshay Jain
// Purpose            : UI for ReactJS calendar
// Last modified      : Aug 03, 2016
-->